var gulp = require('gulp'),
	sass = require('gulp-sass'),
  prefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  cssmin = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
	watch = require('gulp-watch'),	
	rimraf = require('rimraf'),
	browserSync  = require('browser-sync'),  // browserSync  = require('browser-sync').create()
  reload = browserSync.reload;



var path = {
    dist: { 
        css:   'dist/css/',
        js:    'dist/js/',
        html:  'dist/',
        img:   'dist/img/',
        fonts: 'dist/fonts/'
    },
    src: { 
        sass:  'app/style/style.scss',
        js:    'app/js/**/*.js',
        html:  'app/**/*.html',
        img:   'app/img/**/*',
        fonts: 'app/fonts/**/*'
    },
    watch: { 
        sass:  'app/style/**/*.scss',
        js:    'app/js/**/*.js',
        html:  'app/**/*.html',
        img:   'app/img/**/*',
        fonts: 'app/fonts/**/*'
    },
    clean: './dist'
};

////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('browser-sync',
                           ['build'],
                                          function() {
		browserSync({    //browserSync.init
				server: {
						baseDir: "./dist"
				},
        host: 'localhost',
        port: 3000,
				//tunnel: true,  
        //notify: false
				//files: ['dist/**/*.html','dist/js/**/*.js','dist/css/**/*.css']
		});
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('sass', function(){
  return gulp.src(path.src.sass)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefixer())
    .pipe(cssmin({debug: true}, function(details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.dist.css))
    .pipe(reload({stream: true}))
    //.pipe(notify('Sass Done! RUBBER-DUCKS.'))
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('scripts', function() {
  return gulp.src(path.src.js)
    .pipe(gulp.dest(path.dist.js))
    .pipe(reload({stream: true}))
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('htmldist', function() {
  return gulp.src(path.src.html)
    .pipe(gulp.dest(path.dist.html))
    .pipe(reload({stream: true}))
    //.pipe(notify('HTML Done! RUBBER-DUCKS.'))
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('image', function () {
  return gulp.src(path.src.img)
        .pipe(imagemin({
          progressive: true,  //.jpg
          svgoPlugins: [{removeViewBox: false}],   //.svg
          interlaced: true,  //.gif
          optimizationLevel: 3 //ст. стиску від 0 до 7
         }))
        .pipe(gulp.dest(path.dist.img))
        .pipe(reload({stream: true}))        
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('fontsdist', function() {
  return gulp.src(path.src.fonts)   
    .pipe(gulp.dest(path.dist.fonts))
    .pipe(reload({stream: true}))
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('build', ['sass', 'htmldist', 'scripts', 'image', 'fontsdist']);
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('watch', ['browser-sync'], function(){
  gulp.watch(path.watch.sass, ['sass']);
  gulp.watch(path.watch.js, ['scripts']);
  gulp.watch(path.watch.html, ['htmldist']);
  /*gulp.watch(path.watch.img, ['image']);*/
  gulp.watch(path.watch.fonts, ['fontsdist']);
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});
////////////////////////////////SCORPIO79//////////////////////////////////////
gulp.task('default', ['watch']);
////////////////////////////////SCORPIO79//////////////////////////////////////